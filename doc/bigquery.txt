Get accurate GCP billing data for a month:

SELECT
  service.description as service,
  sku.description as sku,
  location.location as location,
  project.id as project,
  (SUM(CAST(cost * 1000000 AS int64))
    + SUM(IFNULL((SELECT SUM(CAST(c.amount * 1000000 as int64))
                  FROM UNNEST(credits) c), 0))) / 1000000
    AS total_exact,
TO_JSON_STRING(labels) as labels
FROM `de-ops-shared-infra.billing.gcp_billing_export_v1_01A2C4_74164C_D4050B`
WHERE invoice.month = "201912"
GROUP BY service, sku, location, project, TO_JSON_STRING(labels)
;
