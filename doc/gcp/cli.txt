# Create control from CLI

gcloud compute instances create de-na-ne1-c002-controller \
        --zone=northamerica-northeast1-b \
        --machine-type=custom \
		--custom-cpu=8 \
		--custom-memory=16 \
        --boot-disk-device-name=de-na-ne1-c002-controller \
		--network=mobius-prod \
		--network-interface=[address=10.162.0.2]

List events / operations

gcloud compute operations list --filter="targetLink:de-eu-w03-c001-node2-4"
