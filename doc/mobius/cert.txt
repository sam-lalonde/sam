On Place59:
`openssl s_client -connect ldaps.husson.edu:636`

Copy all the contents after `Server Certificate`

Paste it into a documement, give it a name like `husson.crt`

Open it up and you can see if it has a valid certificate chain.

you can also check to see if they are also presenting the certificate chain on their server.  look for

```No client certificate CA names sent```

which means they are not presenting any portion of the certificate chain, in which case, hope and pray Java's default trust store has the certs in them
if not, we can create a custom keystore with the certificate and add it