If you can't SSH to a controller it could be that sshguard is locking it out. You can flush it with this command:

iptables -F sshguard