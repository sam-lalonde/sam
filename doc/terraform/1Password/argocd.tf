# Define the external data source to retrieve the admin password from a bash script
# NOTE 1: You need to have the 1Password CLI installed and configured
# NOTE 2: A Login item called (exactly) "argocd-cluster" (e.g. argocd-c900) must exist in 1Password
#         and it must have a custom field called "bcrypt" with a bcrypt hash of the password
data "external" "argocd_admin_password_bcrypt" {
  count   = var.install_argocd ? 1 : 0
  program = ["/bin/bash", "${path.module}/scripts/get_argocd_admin_password.sh", var.cluster_name]
}

# Define the ArgoCD namespace resource
resource "kubernetes_namespace" "argocd" {
  count = var.install_argocd ? 1 : 0
  metadata {
    name = "argocd"
  }
}

# Deploy ArgoCD with Helm
resource "helm_release" "argocd" {
  count            = var.install_argocd ? 1 : 0
  depends_on       = [kubernetes_namespace.argocd]
  name             = "argocd"
  repository       = var.argocd_helm_repo
  chart            = var.argocd_helm_chart
  version          = var.argocd_version
  namespace        = kubernetes_namespace.argocd[count.index].metadata[0].name
  create_namespace = false

  set_sensitive {
    name  = "configs.secret.argocdServerAdminPassword"
    value = data.external.argocd_admin_password_bcrypt[count.index].result["bcrypt"]
  }

  values = [
    templatefile("${path.module}/argocd-values.yaml", {
      cluster_name = var.cluster_name
    })
  ]
}
