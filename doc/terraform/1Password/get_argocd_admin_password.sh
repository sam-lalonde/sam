#!/bin/bash

# exit on any error
set -e

# set the cluster
CLUSTER="$1"

# get the password from 1Password
PASSWORD=$(op item get argocd-"${CLUSTER}" --fields bcrypt)

# output the password value as JSON
echo '{ "bcrypt": "'"$PASSWORD"'" }'
