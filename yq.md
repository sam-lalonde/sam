# yq examples
# https://github.com/mikefarah/yq/

kubectl get pod buster-webapp-f7d68fffb-nd8st -o yaml | yq r -p pv - 'metadata.labels'

CONTEXT=`cat ~/.kube/config | yq r -p v - 'current-context'`
NAMESPACE=`cat .kube/config | yq r -p v - 'contexts(name=='"${CONTEXT}"').context.namespace'`
