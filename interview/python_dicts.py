#!/usr/bin/env python3

colour_scores = {"blue": 2, "red": 5}
colour_scores["yellow"] = 4
colour_scores["blue"] += 3

answer1 = colour_scores["red"]
answer2 = colour_scores["blue"]
answer3 = colour_scores.keys()
answer4 = type(colour_scores)

print("Answer 1: {}".format(answer1))
print("Answer 2: {}".format(answer2))
print("Answer 3: {}".format(answer3))
print("Answer 4: {}".format(answer4))
