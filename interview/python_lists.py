#!/usr/bin/env python3

animals = ["cat", "dog", "duck", "monkey", "snake"]
animals.append("horse")

answer1 = animals[2]
answer2 = animals[-1]
answer3 = [x for x in animals if len(x) > 4]

print("Answer 1: {}".format(answer1))
print("Answer 2: {}".format(answer2))
print("Answer 3: {}".format(answer3))
