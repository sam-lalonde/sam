#!/usr/bin/env python

import os
import yaml
import subprocess
import re

repo = "argo-prod"

for c in os.listdir(f"../{repo}/clusters"):
    for t in os.listdir(f"../{repo}/clusters/{c}/values/mobius-tenant/"):
        status, configmap1 = subprocess.getstatusoutput(f"helm template ./mobius-tenant -f ../{repo}/clusters/{c}/values/mobius-tenant/{t} --show-only templates/configmap.yaml | yq .data.tenant_json | jq .pl")
        configmap2 = configmap1.replace('"', "")
        configmap3 = configmap2.replace(',', "")
        configmap4 = configmap3.replace('  ', "")
        configmap5 = configmap4.replace('}', "")
        configmap6 = configmap5.replace('{', "")
        # configmap7 = re.sub(r'[^\x20-\x7E]+', '', configmap6)
        # print(configmap)
        status, tenant1 = subprocess.getstatusoutput(f"helm template ./mobius-tenant -f ../{repo}/clusters/{c}/values/mobius-tenant/{t} --show-only templates/tenant.yaml | yq .spec.pl")
        tenant2 = tenant1.replace('"', "")
        tenant3 = tenant2.replace(',', "")
        configmap = [x for x in configmap6.splitlines() if x]
        tenant = tenant3.splitlines()
        if configmap == tenant:
            pass
            print("They match!")
        else:
          print(f"{t} doesn't match!")
        #   set_diff = list(set(configmap).symmetric_difference(tenant))
        #   print(set_diff)
        #   exit()
        #   print(configmap3)
        #   print()
        #   print(tenant1)
        #   exit()
