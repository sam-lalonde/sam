#!/usr/bin/env python

import os
import yaml

for c in os.listdir("./clusters"):
    print(c)
    for t in os.listdir(f"./clusters/{c}/applications/mobius-tenant/"):
        if t == ".gitkeep":
            continue
        print(t)
        with open(f"./clusters/{c}/applications/mobius-tenant/{t}") as f:
            cur_yaml = yaml.safe_load(f)
        if cur_yaml["metadata"]["annotations"]["fast-lane-wave"] in ("wave2", "wave2-notify"):
            continue
        values_object = cur_yaml["spec"]["sources"][1]["helm"]["valuesObject"]
        if "bt" in values_object:
            if "deploymentAnnotations" in values_object["bt"]:
                del values_object["bt"]["deploymentAnnotations"]
            if values_object["bt"] == {}:
                del values_object["bt"]
        if "annotations" in values_object["ingress"]:
            if "argocd.argoproj.io/sync-wave" in values_object["ingress"]["annotations"]:
                del values_object["ingress"]["annotations"]["argocd.argoproj.io/sync-wave"]
            if values_object["ingress"]["annotations"] == {}:
                del values_object["ingress"]["annotations"]
        cur_yaml["spec"]["sources"][1]["helm"]["valuesObject"] = values_object
        new_yaml = yaml.dump(cur_yaml, default_flow_style=False, allow_unicode=True)
        with open(f"./clusters/{c}/applications/mobius-tenant/{t}", "w") as f:
            f.write(new_yaml)
