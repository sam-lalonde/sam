#!/bin/bash

# Set variables
IMAGE_NAME="shib-idp"
TAGS=("a6f7c72" "latest" "master") # List a few recent tags here, quoted and space-separated

PROJECT_ID="de-eng-mobius-shared"
BUCKET_NAME="de-ops-mobius-backups-na-monthly"

# Loop over each tag
for TAG in "${TAGS[@]}"; do
    TAR_FILE_NAME="${IMAGE_NAME}_${TAG}.tar"

    # Pull the image from Google Container Registry
    docker pull gcr.io/${PROJECT_ID}/${IMAGE_NAME}:"${TAG}"

    # Save the image to a tar file
    docker save -o "${TAR_FILE_NAME}" gcr.io/${PROJECT_ID}/${IMAGE_NAME}:"${TAG}"

    # Upload the tar file to Google Cloud Storage
    gsutil cp "${TAR_FILE_NAME}" gs://${BUCKET_NAME}/gcr-backup/${IMAGE_NAME}/"${TAR_FILE_NAME}"

    # Optional: Clean up the local tar file
    rm "${TAR_FILE_NAME}"

    echo "Image with tag ${IMAGE_NAME}:${TAG} has been successfully exported and uploaded."
done
