#!/usr/bin/env python3
import datetime
import subprocess
import yaml

status, output = subprocess.getstatusoutput("ssh rundeck.digitaled.tools 'rd executions query -p mobius_prod -i a1c613a2-9f9c-4add-bb88-7b3ac49a13e9 --noninteractive --autopage -s succeeded'")
lines = output.splitlines()
for line in lines:
    id = line.split()[0]
    status, output = subprocess.getstatusoutput("ssh rundeck.digitaled.tools 'rd executions info -e {} -v'".format(id))
    idlines = output.splitlines()
    part1 = [x for x in idlines if "argstring" in x][0].split(" -")
    tenant = [x for x in part1 if x.startswith("tenant ")][0].split()[1]
    datestarted = [x for x in idlines if "dateStarted" in x][0].split()[1]
    start = datetime.datetime.strptime(datestarted, "%Y-%m-%dT%H:%M:%SZ")
    dateended = [x for x in idlines if "dateEnded" in x][0].split()[1]
    end = datetime.datetime.strptime(dateended, "%Y-%m-%dT%H:%M:%SZ")
    duration = end - start
    print("{},{}".format(tenant,duration))

