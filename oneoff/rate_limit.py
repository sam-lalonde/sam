import requests
import sys
from datetime import datetime

url = sys.argv[1]

count = 100
i = 1
while i <= count:
    r = requests.get(url)
    print(f"{i} {datetime.now()} {r.status_code}")
    i += 1
