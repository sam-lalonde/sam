#!/usr/bin/env python
"""
tenant_delete_uptimerobot_monitor.py
This script implements functionality to delete a monitor in Uptime Robot.
Monitors are deleted if they match the URL supplied in the Jira ticket.
Deleted monitor information is commented in Jira ticket.
"""
import logging
import os
from modules import common_jira, common_uptimerobot

import requests
# Uptime Robot API constants
API_KEY = "ur880455-fea1d5a4c0e655191dea72a2"
GET_MONITORS_URL = "https://api.uptimerobot.com/v2/getMonitors"
DELETE_MONITOR_URL = "https://api.uptimerobot.com/v2/deleteMonitor"
DEFAULT_HEADERS = {
    "content-type": "application/x-www-form-urlencoded",
    "cache-control": "no-cache",
}
logging.basicConfig(
    format="%(asctime)s - %(levelname)s - %(message)s", level=logging.INFO
)
def delete_monitor(api_key, monitor_id):
    """Delete a Monitor"""
    payload = f"api_key={api_key}&format=json&id={monitor_id}"
    response = requests.post(DELETE_MONITOR_URL, data=payload, headers=DEFAULT_HEADERS)
    response = response.json()
    status = response["stat"]
    return status


def main():
    delete_monitor()

if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s - %(levelname)s - %(message)s", level=logging.INFO
    )
    main()
