#!/usr/bin/env python

import os
import yaml

for c in os.listdir("./clusters"):
    for t in os.listdir(f"./clusters/{c}/applications/mobius-tenant/"):
        if t == ".gitkeep":
            continue
        with open(f"./clusters/{c}/applications/mobius-tenant/{t}") as f:
            cur_yaml = yaml.safe_load(f)

        if cur_yaml["metadata"]["annotations"]["fast-lane-wave"] not in ("wave1", "wave2", "wave2-notify"):
            continue
        cur_yaml["spec"]["sources"][1]["targetRevision"] = "0.35.4"
        new_yaml = yaml.dump(cur_yaml, default_flow_style=False, allow_unicode=True)
        with open(f"./clusters/{c}/applications/mobius-tenant/{t}", "w") as f:
            f.write(new_yaml)
