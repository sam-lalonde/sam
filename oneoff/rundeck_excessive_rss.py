#!/usr/bin/python3
from collections import defaultdict
from pprint import pprint
import operator
import subprocess


TIME = "2w"  # 2 weeks
output = subprocess.getoutput("rd executions query -p mobius_prod -i 4826c096-24c5-4516-b31c-09459f319cba --noninteractive --autopage --outformat %id --recent {}".format(TIME))
job_ids = output.splitlines()
count = defaultdict(int)
for i, job_id in enumerate(job_ids):
    print("Processing {}/{}".format(i+1, len(job_ids)))
    output = subprocess.getoutput("rd executions follow --tail 1000 -e {}".format(job_id))
    lines = output.splitlines()
    summary = [x for x in lines if "Summary of instances to restart" in x][0]
    instances = list(eval(summary.split("restart: ")[1].strip()).keys())
    for instance in instances:
        count[instance] += 1
sorted_count = sorted(count.items(), key=operator.itemgetter(1), reverse=True)
for k, v in sorted_count:
    print("{}: {}".format(v, k))
