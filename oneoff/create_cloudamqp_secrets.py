#!/usr/bin/env python3
import subprocess
import sys

cluster = sys.argv[1]
instance = sys.argv[2]


def create_cloudamqp_secrets():
    # check if secret already exists
    exitcode, _output = subprocess.getstatusoutput(
        "kubectl -n {0} get secret {0}-cloudamqp-secrets".format(instance)
    )
    if exitcode == 0:
        print(
            "CloudAMQP secret already exists for {}, not overwriting".format(instance)
        )
        return True
    # get url
    exitcode, output = subprocess.getstatusoutput(
        "kubectl get secrets/cloudamqp-{0}-0-secrets "
        "--template={{{{.data.dmsRabbitMqUri}}}} | base64 -d".format(
            cluster
        )
    )
    if exitcode != 0:
        print("ERROR: Unable to get dmsRabbitMqUri")
        print(output)
        return False
    # create secret
    dmsRabbitMqUri = output
    exitcode, output = subprocess.getstatusoutput(
        "kubectl -n {0} create secret generic {0}-cloudamqp-secrets "
        "--from-literal=dmsRabbitMqUri={1}".format(
            instance, dmsRabbitMqUri
        )
    )
    if exitcode != 0:
        print("ERROR: Unable to create secret")
    print(output)


create_cloudamqp_secrets()
