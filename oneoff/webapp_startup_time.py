#!/usr/bin/env python3
import datetime
import subprocess
import yaml


status, output = subprocess.getstatusoutput("kubectl get pods -A | grep webapp")
startup_times = []
for line in output.splitlines():
    ns, pod = line.split()[0:2]
    print("Processing {}".format(ns))
    status, output = subprocess.getstatusoutput("kubectl -n {} get pod {} -o yaml".format(ns, pod))
    pod_yaml = yaml.safe_load(output)
    conditions = pod_yaml["status"]["conditions"]
    for c in conditions:
        if c["type"] == "Initialized":
            initialized = datetime.datetime.strptime(c["lastTransitionTime"], '%Y-%m-%dT%H:%M:%SZ')
            start_date = initialized.date()
        elif c["type"] == "Ready":
            ready = datetime.datetime.strptime(c["lastTransitionTime"], '%Y-%m-%dT%H:%M:%SZ')
    startup_time = ready - initialized
    startup_times.append("{} {} {}".format(startup_time, start_date, ns))
for i in sorted(startup_times):
    print(i)
