#!/usr/bin/env python

import os
import yaml


i = 1
for c in os.listdir("./clusters"):
    for t in os.listdir(f"./clusters/{c}/values/mobius-tenant/"):
        with open(f"./clusters/{c}/values/mobius-tenant/{t}") as f:
            cur_yaml = yaml.safe_load(f)

        if "bt" not in cur_yaml:
            cur_yaml["bt"] = {}
        if "deploymentAnnotations" not in cur_yaml["bt"]:
            cur_yaml["bt"]["deploymentAnnotations"] = {}
        cur_yaml["bt"]["deploymentAnnotations"]["argocd.argoproj.io/sync-wave"] = "5"
        if "annotations" not in cur_yaml["ingress"]:
            cur_yaml["ingress"]["annotations"] = {}
        cur_yaml["ingress"]["annotations"]["argocd.argoproj.io/sync-wave"] = "10"


        new_yaml = yaml.dump(cur_yaml, default_flow_style=False, allow_unicode=True)
        with open(f"./clusters/{c}/values/mobius-tenant/{t}", "w") as f:
            f.write(new_yaml)

