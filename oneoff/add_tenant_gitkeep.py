#!/usr/bin/env python

import os
import yaml

ALL_WAVES = ('pre-release', 'internal', 'uw-beta', 'wave0', 'wave1', 'wave2', 'wave2-notify')
WAVES_TO_PROCESS = ('pre-release')

for c in os.listdir("./clusters"):
    for t in os.listdir(f"./clusters/{c}/applications/mobius-tenant/"):
        with open(f"./clusters/{c}/applications/mobius-tenant/{t}", "r") as f:
            cur_yaml = yaml.safe_load(f)
        if cur_yaml["metadata"]["annotations"]["fast-lane-wave"] not in WAVES_TO_PROCESS:
            continue
        tenant = cur_yaml["metadata"]["name"]
        with open(f"./clusters/{c}/manifests/mobius-tenant/{tenant}/.gitkeep", "w") as file:
            pass
        if tenant not in skip_tenants:
            os.remove(f"./clusters/{c}/manifests/mobius-tenant/{tenant}/{tenant}-secrets-ss.yaml")
