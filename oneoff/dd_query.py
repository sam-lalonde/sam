#!/usr/bin/env python3.9
import requests
import time
from pprint import pprint
from datadog import initialize, api

with open("/home/slalonde/tmp/dd-api-key") as f:
    dd_api_key = f.read().strip()
with open("/home/slalonde/tmp/dd-app-key") as f:
    dd_app_key = f.read().strip()

options = {
    "api_key": dd_api_key,
    "app_key": dd_app_key,
}

initialize(**options)
now = int(time.time())
start = now - 60
query = "max:users.active{cloud_provider:gcp} by {school}"
results = api.Metric.query(start=start, end=now, query=query)
pprint(results)
