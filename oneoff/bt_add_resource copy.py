#!/usr/bin/env python

import os
import yaml

MAX_TENANTS = 100
LIMITS = "768Mi"
REQUESTS = "390Mi"

i = 1
for c in os.listdir("./clusters"):
    for t in os.listdir(f"./clusters/{c}/values/mobius-tenant/"):
        if i > MAX_TENANTS:
            exit()
        with open(f"./clusters/{c}/values/mobius-tenant/{t}") as f:
            cur_yaml = yaml.safe_load(f)

        # skip if limit already set
        try:
            cur_limit = cur_yaml["bt"]["resources"]["limits"]
        except KeyError:
            pass
        else:
            continue

        if "bt" not in cur_yaml:
            cur_yaml["bt"] = {}
        if "resources" not in cur_yaml["bt"]:
            cur_yaml["bt"]["resources"] = {}
        cur_yaml["bt"]["resources"]["limits"] = {"memory": LIMITS}
        cur_yaml["bt"]["resources"]["requests"] = {"memory": REQUESTS}
        new_yaml = yaml.dump(cur_yaml, default_flow_style=False, allow_unicode=True)
        with open(f"./clusters/{c}/values/mobius-tenant/{t}", "w") as f:
            f.write(new_yaml)
        i += 1
