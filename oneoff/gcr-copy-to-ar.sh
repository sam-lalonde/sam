#!/bin/bash

# Set variables
IMAGE_NAME="background-tasks"
TAGS=("2024.2-c35d044") # List a few recent tags here, quoted and space-separated

PROJECT_ID="de-eng-mobius-shared"

# Loop over each tag
for TAG in "${TAGS[@]}"; do
    gcrane cp \
        gcr.io/${PROJECT_ID}/${IMAGE_NAME}:"${TAG}" \
        us-docker.pkg.dev/${PROJECT_ID}/images/${IMAGE_NAME}:"${TAG}"

    echo "Image with tag ${IMAGE_NAME}:${TAG} has been successfully copied to Artifact Registry."
done
