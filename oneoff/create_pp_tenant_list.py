#!/usr/bin/env python

import os
import yaml


pp_tenants = []
for c in os.listdir("./clusters"):
    if c != "c002":
        continue
    for t in os.listdir(f"./clusters/{c}/values/mobius-tenant/"):
        if t == ".gitkeep":
            continue
        with open(f"./clusters/{c}/values/mobius-tenant/{t}") as f:
            cur_yaml = yaml.safe_load(f)
        tenant_id = cur_yaml["global"]["tenantID"]
        try:
            pp = cur_yaml["props"]["ffNewLicensing"]
        except:
            pp = False
        if pp:
            pp_tenants.append(tenant_id)
print(",".join(sorted(pp_tenants)))
