#!/usr/bin/env python

import os
import yaml

for c in os.listdir("./clusters"):
    for t in os.listdir(f"./clusters/{c}/applications/mobius-tenant/"):
        if t == ".gitkeep":
            continue
        with open(f"./clusters/{c}/applications/mobius-tenant/{t}") as f:
            cur_yaml = yaml.safe_load(f)
        # if cur_yaml["metadata"]["annotations"]["fast-lane-wave"] not in ("wave2", "wave2-notify"):
        #     continue
        values = t.replace("argo", "values")
        with open(f"./clusters/{c}/values/mobius-tenant/{values}") as f:
            cur_values_yaml = yaml.safe_load(f)
        if "bt" not in cur_values_yaml:
            cur_values_yaml["bt"] = {}
        if "deploymentAnnotations" not in cur_values_yaml["bt"]:
            cur_values_yaml["bt"]["deploymentAnnotations"] = {}
        cur_values_yaml["bt"]["deploymentAnnotations"]["argocd.argoproj.io/sync-wave"] = "5"
        if "annotations" not in cur_values_yaml["ingress"]:
            cur_values_yaml["ingress"]["annotations"] = {}
        cur_values_yaml["ingress"]["annotations"]["argocd.argoproj.io/sync-wave"] = "10"
        new_values_yaml = yaml.dump(cur_values_yaml, default_flow_style=False, allow_unicode=True)
        with open(f"./clusters/{c}/values/mobius-tenant/{values}", "w") as f:
            f.write(new_values_yaml)
