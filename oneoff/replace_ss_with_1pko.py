#!/usr/bin/env python

import os
import yaml

ALL_WAVES = ('pre-release', 'internal', 'uw-beta', 'wave0', 'wave1', 'wave2', 'wave2-notify')
WAVES_TO_PROCESS = ('pre-release', 'internal', 'uw-beta', 'wave0', 'wave1', 'wave2', 'wave2-notify', 'nowave')

for c in os.listdir("./clusters"):
    for t in os.listdir(f"./clusters/{c}/applications/mobius-tenant/"):
        if t == ".gitkeep":
            continue
        with open(f"./clusters/{c}/applications/mobius-tenant/{t}", "r") as f:
            cur_yaml = yaml.safe_load(f)
        if cur_yaml["metadata"]["annotations"]["fast-lane-wave"] not in WAVES_TO_PROCESS:
            continue
        tenant = cur_yaml["metadata"]["name"]
        if os.path.exists(f"./clusters/{c}/manifests/mobius-tenant/{tenant}/{tenant}-secrets-ss.yaml"):
            cur_yaml["spec"]["sources"][1]["helm"]["valuesObject"]["onePassword"] = {"createSecret": True}
            new_yaml = yaml.dump(cur_yaml, default_flow_style=False, allow_unicode=True)
            with open(f"./clusters/{c}/applications/mobius-tenant/{t}", "w") as f:
                f.write(new_yaml)
            with open(f"./clusters/{c}/manifests/mobius-tenant/{tenant}/.gitkeep", "w") as file:
                pass
            os.remove(f"./clusters/{c}/manifests/mobius-tenant/{tenant}/{tenant}-secrets-ss.yaml")
