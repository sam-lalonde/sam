#!/bin/bash
# WARNING: MAKE SURE YOU KNOW WHAT YOU ARE DOING!!!
# IF YOU USE AN INVALID TARGET CHART VERSION YOU'LL BREAK STUFF
# USAGE: helm_upgrade_chart_version.sh <cluster-id> <start-character-range> [source-chart-major-version] [target-chart-version] ["helm-options"]
# The script needs to be run in the root of the mob-deploy repository. PULL IT FRESH!
# example:
# ./upgrade_chart_version.sh c003 a-z 0.3 0.4.1 --dry-run
#
CLUSTER="$1"
START_CHAR_RANGE="$2"
SOURCE_MAJOR_CHART_VERSION="mobius-${3}"
TARGET_CHART_VERSION="$4"
DRY_RUN="$5"

MOB_NS=($(kubectl get deployments.apps --all-namespaces --selector=tags.datadoghq.com/service=mob-webapp --output=jsonpath='{.items[*].metadata.namespace}' | tr ' ' '\n'))
TO_UPGRADE=()
MOBIUS_CHART="digitaled/mobius"

# make sure the current kubectl context and the targeted cluster match up, or life is gonna get ugly.
result=$(kubectl config current-context | grep $CLUSTER)
if [ "$?" == "1" ]; then
    current_context=$(kubectl config current-context)
    echo "Trying to upgrade charts for cluster $CLUSTER"
    echo "but kubectl is configured for context $current_context"
    exit 1
fi

for ns in "${MOB_NS[@]}"; do
    chart_version=$(helm -n $ns history $ns -o json | jq -r 'sort_by(-.revision) | .[] | select(.status == "deployed")  | .chart' | head -n 1)
    need_upgrade="false"
    upgrade_regex="^${SOURCE_MAJOR_CHART_VERSION}.*$"
     if [ "$chart_version" != "mobius-${TARGET_CHART_VERSION}" ]; then
        if [[ "$chart_version" =~ $upgrade_regex ]]; then
            start_char_regex="^[${START_CHAR_RANGE}]"
            if [[ "$ns" =~ $start_char_regex ]]; then
                need_upgrade="true"
                TO_UPGRADE+=($ns)
            fi
        fi
    fi
    printf "%-30s %-15s  %-5s\n" $ns $chart_version $need_upgrade
done

echo

TO_UPGRADE_COUNT=${#TO_UPGRADE[@]}
for index in "${!TO_UPGRADE[@]}"; do
    instance=${TO_UPGRADE[index]}
    display_index=$((index+=1))
    echo "Upgrading instance $instance to $TARGET_CHART_VERSION ($display_index/$TO_UPGRADE_COUNT)"
    values_file="mobius/${CLUSTER}/${instance}/${instance}-values.yaml"
    if [ ! -f $values_file ]; then
        echo "ERROR: Missing required values file $values_file"
        continue
    fi
    helm -n $instance upgrade $instance $MOBIUS_CHART $DRY_RUN --wait --timeout 10m0s --version $TARGET_CHART_VERSION --reset-values -f $values_file
done
