#!/bin/bash

echo Set zone
gcloud config set compute/zone northamerica-northeast1-b

echo Stop nodes
gcloud compute instances stop de-na-ne1-c901-node-0 de-na-ne1-c901-node-1

echo Stop controllers and load balancers
gcloud compute instances stop de-na-ne1-c901-controller de-na-ne1-c901-lb-0 de-na-ne1-c901-lb-1

echo Stop databases
gcloud compute instances stop de-na-ne1-c901-pg-db-srv-0 de-na-ne1-c901-pg-db-rpl-0

echo Stop storage
gcloud compute instances stop de-na-ne1-c901-storage
