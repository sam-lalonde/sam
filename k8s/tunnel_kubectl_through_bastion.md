You can use this method to tunnel kubectl through bastion so that your local IP address dos not have to be in `master_authorized_networks`.

Adapted from https://cloud.google.com/kubernetes-engine/docs/tutorials/private-cluster-bastion

PREREQUISTE

tinyproxy running on bastion, with "Allow localhost" in its configuration

https://cloud.google.com/kubernetes-engine/docs/tutorials/private-cluster-bastion#deploy-proxy

HOW TO USE ON YOUR LOCAL MACHINE

1) Start a tunnel

Option 1: With SSH

```
ssh bastion -L8888:localhost:8888 -N -q -f
```

Option 2: With gcloud

```
gcloud compute ssh bastion-1 \
    --tunnel-through-iap \
    --project=de-ops-shared-infra \
    --zone=northamerica-northeast1-c \
    -- -4 -L8888:localhost:8888 -N -q -f
```

2) Export HTTPS_PROXY

```
export HTTPS_PROXY=localhost:8888
```

That's it! You can now use `kubectl` normally.

Note: If you have `HTTPS_PROXY=localhost:8888` set, but no tunnel established, that shell is going to be pretty broken.
