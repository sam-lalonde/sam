import asyncio
import time

async def main():
    print('hello')
    await asyncio.sleep(1)
    # await time.sleep(1)
    print('world')

asyncio.run(main())
