#!/usr/bin/env python3
import argparse
import getpass
import os
import time
import yaml


def main():
    user = getpass.getuser()
    filestore_ips = {
        "c900": "10.139.117.26",
        "c001": "10.160.70.58",
        "c002": "10.90.164.218",
        "c003": "10.168.90.10",
        "c004": "096a749222-qts58.cn-hangzhou.nas.aliyuncs.com",
    }
    image_repo = {
        "c900": "us-docker.pkg.dev/de-eng-mobius-shared/images/mob-utils",
        "c001": "us-docker.pkg.dev/de-eng-mobius-shared/images/mob-utils",
        "c002": "us-docker.pkg.dev/de-eng-mobius-shared/images/mob-utils",
        "c003": "us-docker.pkg.dev/de-eng-mobius-shared/images/mob-utils",
        "c004": "registry-intl.cn-hangzhou.aliyuncs.com/de-eng-mobius-shared/mob-utils",
    }
    pv = {
        "apiVersion": "v1",
        "kind": "PersistentVolume",
        "metadata": {"name": "{}-pv-volume".format(user), "namespace": user},
        # "metadata": {"name": "{}-pv-volume".format(user)},
        "spec": {
            "capacity": {"storage": "465Gi"},
            "accessModes": ["ReadWriteMany"],
            "nfs": {"path": "/hosted_apps", "server": filestore_ips[args.cluster]},
        },
    }
    pvc = {
        "apiVersion": "v1",
        "kind": "PersistentVolumeClaim",
        "metadata": {"name": "{}-pv-claim".format(user), "namespace": user},
        # "metadata": {"name": "{}-pv-claim".format(user)},
        "spec": {
            "accessModes": ["ReadWriteMany"],
            "storageClassName": "",
            "resources": {"requests": {"storage": "465Gi"}},
        },
    }

    deployment = {
        "apiVersion": "apps/v1",
        "kind": "Deployment",
        "metadata": {
            "name": "{}-deployment".format(user),
            "namespace": user,
            "labels": {"app": "{}-mob-utils".format(user)},
        },
        # "metadata": {"name": "{}-deployment".format(user), "labels": {"app": "{}-mob-utils".format(user)}},
        "spec": {
            "selector": {"matchLabels": {"app": "{}-mob-utils".format(user)}},
            "template": {
                "metadata": {"labels": {"app": "{}-mob-utils".format(user)}},
                "spec": {
                    "containers": [
                        {
                            "name": "mob-utils",
                            "image": "{}:0.2.23".format(image_repo[args.cluster]),
                            "command": [
                                "tail",
                                "-f",
                                "/dev/null",
                            ],
                            "volumeMounts": [
                                {
                                    "mountPath": "/usr/local/hosted_apps",
                                    "name": "{}-mount".format(user),
                                }
                            ],
                        }
                    ],
                    "volumes": [
                        {
                            "name": "{}-mount".format(user),
                            "persistentVolumeClaim": {
                                "claimName": "{}-pv-claim".format(user)
                            },
                        }
                    ],
                    "nodeSelector": {"default-node-pool": "true"},
                },
            },
        },
    }

    os.system("mkdir /tmp/{}-k8s 2>/dev/null".format(user))
    with open("/tmp/{}-k8s/pv.yaml".format(user), "w") as f:
        yaml.dump(pv, f, default_flow_style=False, allow_unicode=True)
    with open("/tmp/{}-k8s/pvc.yaml".format(user), "w") as f:
        yaml.dump(pvc, f, default_flow_style=False, allow_unicode=True)
    with open("/tmp/{}-k8s/deployment.yaml".format(user), "w") as f:
        yaml.dump(deployment, f, default_flow_style=False, allow_unicode=True)
    os.system("kubectl create namespace {} 2>/dev/null".format(user))
    time.sleep(1)
    os.system("kubectl apply -f /tmp/{0}-k8s".format(user))


def process_args():
    parser = argparse.ArgumentParser(description="Create mob-utils environment")
    parser.add_argument("-c", "--cluster", required=True)
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = process_args()
    main()
