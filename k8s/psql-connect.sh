#!/bin/bash
set -e
IMAGE="postgres:14"

# send help
if [ "$1" = "help" ]
  then
    echo "Usage: psql-connect.sh [-c cluster] [-t tenant] [-h dbhost] [-u dbuser]"
    exit
fi

# process arguments
while getopts ':c:t:h:u:' c
do
  case $c in
    c) CLUSTER=${OPTARG} ;;
    t) TENANT=${OPTARG} ;;
    h) DBHOST=${OPTARG} ;;
    u) DBUSER=${OPTARG} ;;
  esac
done

# set variables if not received in arguments
if [ -z $CLUSTER ]; then
  echo -n "Cluster: "
  read CLUSTER
fi
if [ -z $TENANT ]; then
  echo -n "Möbius instance_id or tenant_id: "
  read TENANT
fi
if [ -z $DBUSER ]; then
  echo -n "DB Host: "
  read DBHOST
fi
if [ -z $DBHOST ]; then
  echo -n "DB User: "
  read USER
fi

# get password
echo -n "Password: "
read -s PASSWORD


# confirm gcloud authorizaton, login if necessary
echo -e "\n\nChecking gcloud authorization"
EMAIL="`echo $USER | awk -F'_digitaled_com' '{print $1}'`@digitaled.com"
if gcloud auth list --quiet --filter-account=${EMAIL} | grep ^'*'; then
    echo -e "gcloud authentication looks good!\n"
else
    gcloud auth login $EMAIL
fi

# set kubectl context
if [ $CLUSTER = "c900" ]; then
  gcloud container clusters get-credentials c900 --zone northamerica-northeast1-b --project de-dev-mobius-test
elif [ $CLUSTER = "c001" ]; then
  gcloud container clusters get-credentials c001 --zone europe-west3-b --project de-ops-mobius-prod
elif [ $CLUSTER = "c002" ]; then
  gcloud container clusters get-credentials c002 --zone northamerica-northeast1-b --project de-ops-mobius-prod
elif [ $CLUSTER = "c003" ]; then
  gcloud container clusters get-credentials c003 --zone australia-southeast1-b --project de-ops-mobius-prod
else
  echo -e "\nERROR: Unknown cluster"
  exit 1
fi

# connect to the database
echo -e "\nConnecting to database..."
kubectl run `whoami`-postgres -i --tty --restart=Never --rm \
 --image=${IMAGE} \
 --env="PGPASSWORD=${PASSWORD}" \
 -- \
 psql \
 -h ${DBHOST} \
 -d ${TENANT} \
 -U ${DBUSER}
