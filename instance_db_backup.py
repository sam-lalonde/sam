#!/usr/bin/python3
import argparse
import os


def main():
    """ Execute the rundeck 'Point-in-Time DB Backup' job """
    os.system("rd run --project mobius_{} --job \"Point-in-Time DB Backup\" --follow -- -instanceName {}".format(args.project, args.instance))


def process_args():
    parser = argparse.ArgumentParser(description="Point-in-Time DB Backup")
    parser.add_argument("--instance", "-i", required=True ,help="Möbius instance")
    parser.add_argument("--project", "-p", choices=["dev", "stg", "prod"], required=True, help="Rundeck Project")
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = process_args()
    main()
