#!/Users/sam/venv/3.11-1/bin/python
import os
import time

import bitbucket_utils
import yaml

repo = "sam-lalonde/sam"
source_branch = "main"

client_id = os.getenv("BB_OAUTH_CLIENT_ID")
client_secret = os.getenv("BB_OAUTH_CLIENT_SECRET")

# authenticate
access_token = bitbucket_utils.authenticate_bitbucket_api(client_id, client_secret)
headers = {
    "Authorization": "Bearer {}".format(access_token),
}

# get files
file_paths = [
    "bitbucket/file1.yaml",
    "bitbucket/file2.yaml",
]
files = bitbucket_utils.get_repository_files(repo, headers, source_branch, file_paths)

# write new files
new_files = []
for i, file_path in enumerate(file_paths):
    cur_yaml = yaml.safe_load(files[i])
    cur_yaml["count"] += 1
    new_yaml = yaml.dump(cur_yaml, default_flow_style=False, allow_unicode=True)
    new_files.append(
        {
            "path": file_path,
            "content": new_yaml,
        }
    )

# create branch
new_branch = f"update-files-{time.time()}"
bitbucket_utils.create_branch(repo, new_branch, source_branch, headers)

# create commit
message = "hello world"
bitbucket_utils.create_commit(repo, new_branch, new_files, message, headers)

# create pull request
title = "Sam pull request"
description = "x"
bb_reviewers = ["{e2a1bd96-d9de-43a9-90ef-fb945f26f0f2}"]
pull_request_id = bitbucket_utils.create_pull_request(
    repo, new_branch, title, description, bb_reviewers, headers
)

# merge pull request
bitbucket_utils.merge_pull_request(repo, pull_request_id, message, headers)
