import json
import time

import requests


def authenticate_bitbucket_api(client_id, client_secret):
    """
    Authenticate with Bitbucket API and obtain an access token.

    :param client_id: The client ID for Bitbucket API.
    :param client_secret: The client secret for Bitbucket API.
    :return: The access token if authentication is successful,
             otherwise exit with status code 1.
    """
    print("Authenticating Bitbucket API")
    data = {"grant_type": "client_credentials"}
    response = request_with_retry(
        requests.post,
        "https://bitbucket.org/site/oauth2/access_token",
        auth=(client_id, client_secret),
        data=data,
    )
    if "access_token" in response.json():
        access_token = response.json().get("access_token")
        print("Bitbucket access token obtained")
        return access_token
    else:
        print("Unable to get access token")
        exit(1)


def get_repository_files(repo, headers, branch, file_paths):
    """
    Retrieve file contents from a Bitbucket repository.

    :param repo: The repository identifier, formatted as "account/repo_slug".
    :param headers: The request headers.
    :param branch: The branch to retrieve files from.
    :param file_paths: A list of file paths to retrieve.
    :return: A list of file contents.
    """
    print("Retrieving file(s) from Bitbucket")
    headers["Content-Type"] = "application/json"
    files = []
    for file_path in file_paths:
        url = (
            f"https://api.bitbucket.org/2.0/repositories/{repo}/src/"
            f"{branch}/{file_path}"
        )
        response = request_with_retry(requests.get, url, headers=headers)
        if response.status_code == 200:
            contents = response.content.decode("utf-8")
            files.append(contents)
        else:
            print(f"Error: Failed to retrieve file {file_path}")
            exit(1)
    print("Files retrieved successfully")
    return files


def create_branch(repo, branch_name, source_branch, headers):
    """
    Create a new branch in a Bitbucket repository.

    :param repo: The repository identifier, formatted as "account/repo_slug".
    :param branch_name: The name of the new branch.
    :param source_branch: The branch to base the new branch on.
    :param headers: The request headers.
    """
    print("Creating new branch")
    headers["Content-Type"] = "application/json"
    url = f"https://api.bitbucket.org/2.0/repositories/{repo}/refs/branches"
    data = {
        "name": branch_name,
        "target": {
            "hash": source_branch,
        },
    }
    response = request_with_retry(requests.post, url, headers=headers, json=data)
    if response.status_code == 201:
        print(f"Branch {branch_name} created successfully")
    else:
        print(f"Error creating branch {branch_name}: {response.text}")
        exit(1)


def create_commit(repo, branch, files, message, headers):
    """
    Commit files to a branch in a Bitbucket repository.
    Note: this method is not suitable for binary or large files.

    :param repo: The repository identifier, formatted as "account/repo_slug".
    :param branch: The branch to commit the files to.
    :param files: A list of dictionaries containing the file path and content.
    :param message: The commit message.
    :param headers: The request headers.
    """
    print("Committing file(s) to branch")
    headers["Content-Type"] = "application/x-www-form-urlencoded"
    url = f"https://api.bitbucket.org/2.0/repositories/{repo}/src"
    data = {
        "branch": branch,
        "message": message,
    }
    for file in files:
        data[file["path"]] = file["content"]

    # Send the API request to create the commit
    response = request_with_retry(requests.post, url, data=data, headers=headers)

    # Check the response status
    if response.status_code == 201:
        print("Files added and commit created successfully")
    else:
        print(f"Error: {response.status_code} - {response.text}")
        exit(1)


def create_pull_request(repo, target_branch, title, description, bb_reviewers, headers):
    """
    Create a pull request in a Bitbucket repository.

    :param repo: The repository identifier, formatted as "account/repo_slug".
    :param target_branch: The target branch for the pull request.
    :param title: The pull request title.
    :param description: The pull request description.
    :param bb_reviewers: A list of Bitbucket reviewer UUIDs.
    :param headers: The request headers.
    :return: The ID of the created pull request.
    """
    print("Creating pull request")
    headers["Content-Type"] = "application/json"
    url = f"https://api.bitbucket.org/2.0/repositories/{repo}/pullrequests"
    reviewers = []
    for reviewer in bb_reviewers:
        reviewers.append({"uuid": reviewer})
    data = {
        "title": title,
        "description": description,
        "source": {"branch": {"name": target_branch}},
        "close_source_branch": "true",
        "reviewers": reviewers,
    }
    response = request_with_retry(
        requests.post,
        url,
        headers=headers,
        json=data,
    )
    # Check the response status
    if response.status_code == 201:
        pull_request_id = response.json()["id"]
        print(f"Pull request #{pull_request_id} created successfully")
        return response.json()["id"]
    else:
        print(f"Error: {response.status_code} - {response.text}")
        exit(1)


def merge_pull_request(repo, pull_request_id, message, headers):
    """
    Merge a pull request in a Bitbucket repository.

    :param repo: The repository identifier, formatted as "account/repo_slug".
    :param pull_request_id: The ID of the pull request to merge.
    :param message: The merge commit message.
    :param headers: The request headers.
    """
    print("Merging pull request")
    headers["Content-Type"] = "application/json"
    url = (
        f"https://api.bitbucket.org/2.0/repositories/{repo}/pullrequests/"
        f"{pull_request_id}/merge"
    )
    data = json.dumps(
        {
            "message": message,
            "strategyId": "squash",
        }
    )

    # Send the API request to merge the pull request
    response = request_with_retry(requests.post, url, data=data, headers=headers)

    # Check the response status
    if response.status_code == 200:
        print(f"Pull request #{pull_request_id} merged successfully")
    else:
        print(f"Error: {response.status_code} - {response.text}")
        try:
            print("Error details:", response.json())
        except ValueError:
            print("No additional error details available.")


def request_with_retry(method, url, auth=None, data=None, json=None, headers=None):
    """
    Send an HTTP request with retries in case of failure.

    :param method: The HTTP method (e.g., requests.get, requests.post).
    :param url: The request URL.
    :param auth: The authentication tuple (e.g., (client_id, client_secret)).
    :param data: The request data.
    :param json: The request JSON data.
    :param headers: The request headers.
    :return: The HTTP response.
    :raises requests.exceptions.HTTPError: If the request fails after the
                                           maximum number of retries.
    """
    retries = 3
    retry_delay = 5  # seconds
    for attempt in range(retries):
        response = method(url, auth=auth, data=data, headers=headers, json=json)
        try:
            response.raise_for_status()
            return response
        except requests.exceptions.HTTPError as e:
            if attempt == retries - 1:
                raise e
            print(f"Request failed, retrying in {retry_delay} seconds")
            time.sleep(retry_delay)
