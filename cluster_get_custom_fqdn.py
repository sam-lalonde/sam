#!/usr/bin/env python3
import logging
import os
import sys
import time
import yaml

MOB_DEPLOY_PATH = "/home/slalonde/mob-deploy"

logging.basicConfig(
    format="%(asctime)s - %(levelname)s - %(message)s", level=logging.INFO
)


def main():
    cluster = sys.argv[1]
    fqdns = []
    st_instances = os.listdir("{}/mobius/{}".format(MOB_DEPLOY_PATH, cluster))
    for i in st_instances:
        with open("{}/mobius/{}/{}/{}-values.yaml".format(MOB_DEPLOY_PATH, cluster, i, i)) as f:
            cur_yaml = yaml.safe_load(f)
            fqdn = cur_yaml["global"]["fqdn"]
            fqdns.append(fqdn)
    tenants = os.listdir("{}/mobius-tenant/{}".format(MOB_DEPLOY_PATH, cluster))
    for i in tenants:
        with open("{}/mobius-tenant/{}/{}/{}-values.yaml".format(MOB_DEPLOY_PATH, cluster, i, i)) as f:
            cur_yaml = yaml.safe_load(f)
        fqdn = cur_yaml["global"]["fqdn"]
        fqdns.append(fqdn)
    fqdns = [ x for x in fqdns if "mobius.cloud" not in x]
    fqdns = [ x for x in fqdns if "mapleserver.com" not in x]
    for i in fqdns:
        print(i)


if __name__ == "__main__":
    main()
