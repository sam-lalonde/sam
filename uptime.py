#!/usr/bin/env python3
import logging
import requests
import sys
import time

logging.basicConfig(
    format="%(asctime)s - %(levelname)s - %(message)s", level=logging.INFO
)


def main():
    url = sys.argv[1]
    while True:
        r = requests.get(url)
        logging.info("{} {:.3f}".format(r.status_code, r.elapsed.total_seconds()))
        time.sleep(1)


if __name__ == "__main__":
    main()
