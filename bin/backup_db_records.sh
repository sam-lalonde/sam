#!/bin/bash

tenants=( mladen sam )
for i in "${tenants[@]}"
do
    echo "$(date)" "STARTING BACKUP FOR $i"
    ./db.sh "$i" & PIDDB=$!
    ./rec.sh "$i" & PIDREC=$!
    wait $PIDDB
    wait $PIDREC
    echo "$(date)" "COMPLETED BACKUP FOR $i"
done
