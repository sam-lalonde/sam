#!/usr/bin/python3
import os

import yaml

# make tenants dict
with open(
    "/Users/sam/repos/sre-puppet/modules/mapleta/files/automation/config.yaml", "r"
) as f:
    config_yaml = yaml.safe_load(f)
fast_lane = config_yaml["fast_lane_tenants"]
waves = list(fast_lane.keys())
tenants = {}
for wave in waves:
    for tenant in fast_lane[wave]:
        tenants[tenant] = wave

# populate values.yaml
for c in ("c001", "c002", "c003"):
    for t in os.listdir("/Users/sam/repos/mob-deploy/mobius-tenant/{}".format(c)):
        wave = False
        if t in tenants:
            wave = tenants[t]
        with open(
            "/Users/sam/repos/mob-deploy/mobius-tenant/{0}/{1}/{1}-values.yaml".format(
                c, t
            ),
            "r",
        ) as f:
            tenant_yaml = yaml.safe_load(f)
        tenant_yaml["fastLane"] = {"wave": wave}
        with open(
            "/Users/sam/repos/mob-deploy/mobius-tenant/{0}/{1}/{1}-values.yaml".format(
                c, t
            ),
            "w",
        ) as f:
            yaml.dump(tenant_yaml, f, default_flow_style=False, allow_unicode=True)
