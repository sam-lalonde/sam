#!/Users/sam/venv/3.12.0/bin/python
import logging
import os
from collections import defaultdict

from kubernetes import client, config


def main():
    node_allocatable = {}
    node_requests = defaultdict(int)
    config.load_kube_config()
    proxy_url = os.getenv("HTTPS_PROXY", None)
    if proxy_url:
        client.Configuration._default.proxy = proxy_url
    v1 = client.CoreV1Api()
    node_list = v1.list_node()
    for node in node_list.items:
        node_name = node.metadata.name
        if "default-node-pool" not in node_name:
            continue
        allocatable_str = node.status.allocatable["memory"]
        if allocatable_str.endswith("Ki"):
            gib = int(allocatable_str.split("Ki")[0]) / 1024 / 1024
        else:
            logging.error("Unregonized units {}".format(allocatable_str))
            exit(1)
        node_allocatable[node_name] = gib
    pods = v1.list_pod_for_all_namespaces(pretty=True)
    for pod in pods.items:
        if pod.status.phase != "Running":
            continue
        pod_node = pod.spec.node_name
        if "default-node-pool" not in pod_node:
            continue
        for c in pod.spec.containers:
            if not c.resources.requests:
                continue
            memory_requests_str = c.resources.requests.get("memory")
            if memory_requests_str:
                if memory_requests_str.endswith("Mi"):
                    mi = int(memory_requests_str.split("Mi")[0])
                    memory_requests_gib = mi / 1024
                elif memory_requests_str.endswith("M"):
                    m = int(memory_requests_str.split("M")[0])
                    memory_requests_gib = m / 1.049 / 1024
                elif memory_requests_str.endswith("Gi"):
                    gi = int(memory_requests_str.split("Gi")[0])
                    memory_requests_gib = gi
                else:
                    raise Exception("Unrecognized units {}".format(memory_requests_str))
                node_requests[pod_node] += memory_requests_gib
    print(
        "\n{:<42} {} {} {:^5} {}".format(
            "NODE", "ALLOCATABLE", "REQUESTED", "PCT", "FREE"
        )
    )
    total_allocatable = 0
    total_requested = 0
    for node, requests in node_requests.items():
        total_allocatable += node_allocatable[node]
        total_requested += requests
        print(
            "{} {:^11.1f} {:^9.1f} {:.1%} {:^5.1f}".format(
                node,
                node_allocatable[node],
                requests,
                requests / node_allocatable[node],
                node_allocatable[node] - requests,
            )
        )
    print("{}".format(75 * "-"))
    print(
        "{:<42} {:^11.1f} {:^9.1f} {:.1%} {:^5.1f}".format(
            "Total",
            total_allocatable,
            total_requested,
            total_requested / total_allocatable,
            total_allocatable - total_requested,
        )
    )
    print("(all units GiB)")


if __name__ == "__main__":
    main()
