#!/bin/bash

echo "$(date)" "Starting pg_dump for $1"
sleep 1
echo "$(date)" "Completed pg_dump for $1"
sleep 1
echo "$(date)" "Starting pg_dump copy to bucket for $1"
sleep 1
echo "$(date)" "Completed pg_dump copy to bucket for $1"
sleep 1
echo "$(date)" "Starting removal of local pg_dump for $1"
sleep 1
echo "$(date)" "Completed removal of local pg_dump for $1"
