#!/usr/bin/python3
import os
import time
from git import Repo

TAG = os.getenv("MOB_DEPLOY_TAG", "mob-sync")
COMMIT_ID = os.environ.get("BITBUCKET_COMMIT")


def main():
    print("STARTING QUEUE PROCESS")
    print("This pipeline will only begin when this commit is the next")
    print("one after the mob-sync tag. If that doesn't happen within")
    print("60 minutes it will exit with error and manual SRE intervention")
    print("will be required.")
    repo = Repo(".")
    for _i in range(0, 60):
        dist_from_tag = int(
            repo.git.rev_list("{}..{}".format(TAG, COMMIT_ID), "--count")
        )
        if dist_from_tag == 0:
            print("ERROR: This commit is behind the tag, cannot proceed!")
            exit(1)
        elif dist_from_tag == 1:
            print("This commit is next, continuing with pipeline!")
            exit(0)
        print(
            "This commit is {} away from {}, trying again in 1 minute".format(
                dist_from_tag, TAG
            )
        )
        time.sleep(5)
        # pull tags only
        repo.remotes.origin.pull("refs/tags/*:refs/tags/* -f")
        # repo.remotes.origin.pull("--tags")
    print("ERROR: Timeout waiting for commit to be next to {}".format(TAG))
    exit(1)


if __name__ == "__main__":
    main()
