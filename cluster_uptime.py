#!/usr/bin/env python3
import logging
import os
import requests
import sys
import time
import yaml

MOB_DEPLOY_PATH = "/home/slalonde/mob-deploy"

logging.basicConfig(
    format="%(asctime)s - %(levelname)s - %(message)s", level=logging.INFO
)


def main():
    cluster = sys.argv[1]
    urls = []
    st_instances = os.listdir("{}/mobius/{}".format(MOB_DEPLOY_PATH, cluster))
    for i in st_instances:
        with open("{}/mobius/{}/{}/{}-values.yaml".format(MOB_DEPLOY_PATH, cluster, i, i)) as f:
            cur_yaml = yaml.safe_load(f)
            fqdn = cur_yaml["global"]["fqdn"]
            if "path" in cur_yaml["global"]:
                url = "https://{}/{}/login".format(fqdn, cur_yaml["global"]["path"])
            else:
                url = "https://{}/login".format(fqdn)
            urls.append(url)
    tenants = os.listdir("{}/mobius-tenant/{}".format(MOB_DEPLOY_PATH, cluster))
    for i in tenants:
        with open("{}/mobius-tenant/{}/{}/{}-values.yaml".format(MOB_DEPLOY_PATH, cluster, i, i)) as f:
            cur_yaml = yaml.safe_load(f)
        fqdn = cur_yaml["global"]["fqdn"]
        url = "https://{}/login".format(fqdn)
        urls.append(url)
    while True:
        up = 0
        for url in urls:
            try:
                r = requests.get(url)
                if r.status_code == 200:
                    up += 1
            except:
                pass
        logging.info("{}/{} instances up".format(up, len(urls)))
        time.sleep(60)


if __name__ == "__main__":
    main()
