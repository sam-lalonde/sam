#!/bin/bash

echo Set zone
gcloud config set compute/zone northamerica-northeast1-b

echo Start storage
gcloud compute instances start de-na-ne1-c901-storage

echo Start databases
gcloud compute instances start de-na-ne1-c901-pg-db-srv-0 de-na-ne1-c901-pg-db-rpl-0

echo Start controllers and load balancers
gcloud compute instances start de-na-ne1-c901-controller de-na-ne1-c901-lb-0 de-na-ne1-c901-lb-1

echo Start nodes
gcloud compute instances start de-na-ne1-c901-node-0 de-na-ne1-c901-node-1
