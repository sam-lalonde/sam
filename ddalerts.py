#!/usr/bin/python3
import ddcreds
import requests
from collections import defaultdict

MONTH = "2019-09"

def main():
    host_names = defaultdict(int)
    alert_names = defaultdict(int)
    url = "https://app.datadoghq.com/report/hourly_data/monitor?api_key={}&application_key={}".format(ddcreds.api_key, ddcreds.app_key)
    response = requests.get(url)
    headings = response.text.splitlines()[0].split(",")
    for line in response.text.splitlines()[1:]:
        splitline = line.split(",")
        if MONTH not in splitline[headings.index("hour")]:
            continue
        # this will collapse alerts that happened in the same hour
        host_names[splitline[headings.index("host_name")]] += 1
        alert_names[splitline[headings.index("alert_name")]] += 1
        # this will include ALL alerts
        #host_names[splitline[headings.index("host_name")]] += int(splitline[headings.index("cnt")])
        #alert_names[splitline[headings.index("alert_name")]] += int(splitline[headings.index("cnt")])
    d = alert_names
    sorted_alert_names = [(k, d[k]) for k in sorted(d, key=d.get, reverse=True)]
    print("alert,count")
    for k, v in sorted_alert_names:
        if k != "":
            print("{},{}".format(k, v))
    d = host_names
    sorted_host_names = [(k, d[k]) for k in sorted(d, key=d.get, reverse=True)]
    print()
    print("host,count")
    for k, v in sorted_host_names:
        if k != "":
            print("{},{}".format(k, v))
main()
