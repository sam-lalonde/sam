#!/usr/bin/python3
import atexit
import os
import psycopg2
import socket
import sys
import logging
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)
from modules import common

ffs_to_delete = [
    "ENABLE_COURSE_MODULE_FILE_UPLOAD",
    "ENABLE_EQUATION_EDITOR_SYNTAX_CHECKER",
    "ENABLE_ANTI_CSRF_TOKENS",
]


def main():
    common.clone_repo(info.jira_ticket)
    atexit.register(common.remove_tmpdir, info.jira_ticket)
    with open(sys.argv[1], "r") as f:
        raw_instances = f.readlines()
    instances = [x.strip() for x in raw_instances]
    for instance in instances:
        logging.info("{} starting".format(instance))
        delete_ffs(instance)
        increment_properties_updated(instance)
        logging.info("Restarting background_tasks")
        os.system("sudo -i docker service update --quiet --force {}_background_tasks".format(instance))


def delete_ffs(instance):
    db_params = common.get_db_params(info.jira_ticket, instance, info.cluster)
    try:
        db = common.Postgres(db_params)
        for ff in ffs_to_delete:
            logging.info("Deleting key from system_properties: {}".format(ff))
            sql_check_cond = "key='{}' AND class_id=0".format(ff)
            key_exists = db.sql_exec(
                "SELECT 1 FROM system_properties WHERE {}".format(sql_check_cond)
            )
            if key_exists:
                db.sql_exec("DELETE FROM system_properties WHERE {} RETURNING *;".format(sql_check_cond))
            else:
                logging.warning(
                    "Key {} with class_id=0 was not found in system_properties!".format(ff)
                )
    except psycopg2.DatabaseError as error:
        logging.error("{}Problem deleting keys from system_properties, check manually!{}".format(common.LogColor.RED.value, common.LogColor.NOCOLOUR.value))
    finally:
        if db is not None:
            db.close()


def increment_properties_updated(instance):
    db_params = common.get_db_params(info.jira_ticket, instance, info.cluster)
    try:
        db = common.Postgres(db_params)
        properties_updated = int(db.sql_exec("SELECT value FROM system_properties WHERE key = 'PROPERTIES_UPDATED';")[0][0])
        logging.info("Incrementing PROPERTIES_UPDATED")
        sql_cmd = "UPDATE system_properties SET value='{}' WHERE key = 'PROPERTIES_UPDATED' RETURNING *;".format(str(properties_updated + 1))
        db.sql_exec(sql_cmd)
    except psycopg2.DatabaseError as error:
            logging.error("{}Problem incementing!{}".format(common.LogColor.RED.value, common.LogColor.NOCOLOUR.value))
    finally:
        if db is not None:
            db.close()


class Info:
    """ Simple container for storing shared information """
    def __init__(self):
        self.jira_ticket = "SO-3981"
        self.cluster = socket.gethostname().split("-")[-2]


if __name__ == "__main__":
    info = Info()
    main()
