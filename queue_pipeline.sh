#####################################################################
# This script blocks a pipeline from running until it is the next one
# after (newer than) the mob-sync tag. In the case of pipeline
# failures, SRE intervention will be required to figure out what
# happened, get the mob-sync tag to the right place, and restart
# pipelines as required.
######################################################################

set -e

max_tries=5
delay_seconds=5
try=1

echo "Starting queueing"

if [ $(git rev-list --count mob-sync..${BITBUCKET_COMMIT}) -eq 0 ]
    then
        echo "mob-sync is too far along for this pipeline to execute"
        exit 1
fi

echo "Waiting for this commit to be 1 away from mob-sync"

while [ $(git rev-list --count mob-sync..${BITBUCKET_COMMIT}) -gt 1 ]
do
  echo -n "Try #$try: "
  echo "mob-sync is $(git rev-list --count mob-sync..${BITBUCKET_COMMIT}) away"
  sleep $delay_seconds
  if [ "$try" == "$max_tries" ]
      then
          echo "Too many tries"
          exit 1
  fi
  try=$((try+1))
  git fetch origin refs/tags/*:refs/tags/* -f
done
echo "This commit is next, proceeding with pipeline!"
