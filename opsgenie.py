#!/usr/bin/env python3
import json
import requests


def main():
    headers = {
	'Authorization': 'GenieKey {}'.format(token),
    }

    params = (
	#('query', 'status:open'),
	('offset', '0'),
	('limit', '100'),
	('sort', 'createdAt'),
	('order', 'desc'),
    )

    data = requests.get('https://api.opsgenie.com/v2/alerts', headers=headers, params=params).json().get("data")
    for d in data:
        print(d.get("createdAt"), d.get("message"))


def get_token():
    with open(".opsgenie-token", "r") as f:
        token = f.read().strip()
    return token


if __name__ == "__main__":
    token = get_token() 
    main()
