#!/usr/bin/env python3
"""
This takes one argument, a bucket path, IN QUOTES.

e.g.

./bucket_prune.py "gs://de-ops-mobius-backups-na"
"""
import datetime
import os
import subprocess
import sys

today = datetime.date.today()
one_year_ago = today - datetime.timedelta(days=365)
days_to_delete = []
day = today + datetime.timedelta(days=1)
while day >= one_year_ago:
    day -= datetime.timedelta(days=1)
    # keep monthlies
    if day.day == 1:
        continue
    # keep weeklies from the past 8 weeks
    elif day.weekday() == 6 and (today - day).days <= 56:
        continue
    # keep dailies from the past 14 days
    elif (today - day).days <= 14:
        continue
    days_to_delete.append(str(day))
bucket = sys.argv[1]
controllers = subprocess.check_output(["gsutil", "ls", bucket]).decode("utf-8").splitlines()
for controller in controllers:
    instances = subprocess.check_output(["gsutil", "ls", controller]).decode("utf-8").splitlines()
    for instance in instances:
        dirs_to_delete = ""
        print("{} processing {}".format(datetime.datetime.now(), instance))
        for day in days_to_delete:
            dirs_to_delete += " {}{}/*.dump".format(instance, day)
        os.system("gsutil -m -q rm -r {}".format(dirs_to_delete))
