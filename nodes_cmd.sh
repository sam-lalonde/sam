#!/bin/bash
NODES=`docker node ls | grep node2 | awk '{print $2}'`
for i in $NODES
  do
    echo -n "$i: ";ssh -n $i "$@"
  done
