#!/usr/bin/env python3
from collections import defaultdict
import os
import yaml


def main():
    versions = defaultdict(int)
    # swarm
    for i in (
        "de-eu-w03-c001.yaml",
        "de-na-ne1-c002.yaml",
        "de-oc-se1-c003.yaml",
        "maplesoft-china-1.mapleta.cn.yaml",
    ):
        full_path = "/home/slalonde/sre-puppet/hieradata/mapleta/{}".format(i)
        with open(full_path, "r") as f:
            y = yaml.safe_load(f)
        for i in y["mapleta"]["instances"]:
            raw_version = y["mapleta"]["instances"][i]["app_image"]
            version = raw_version.split(":")[1]
            versions[version] += 1
    # k8S
    for c in ("c001", "c002", "c003", "c004"):
        try:
            for i in os.listdir("/home/slalonde/mob-deploy/mobius/{}".format(c)):
                with open(
                    "/home/slalonde/mob-deploy/mobius/{0}/{1}/{1}-values.yaml".format(c, i)
                ) as f:
                    y = yaml.safe_load(f)
                    version = y["global"]["label"]
                    versions[version] += 1
        except Exception:
            pass
        if c != "c004":
            for i in os.listdir("/home/slalonde/mob-deploy/mobius-tenant/{}".format(c)):
                with open(
                   "/home/slalonde/mob-deploy/mobius-tenant/{0}/{1}/{1}-values.yaml".format(c, i)
                ) as f:
                    y = yaml.safe_load(f)
                    version = y["global"]["label"]
                    versions[version] += 1
    # output results
    for k, v in sorted(versions.items()):
        print("{}: {}".format(k, v))


if __name__ == "__main__":
    main()
