### Setup CLI
aliyun configure --mode AK

### Get help

aliyun help
aliyun command help

### Get clusters

aliyun cs DescribeClusters | jq '[.[] | {name: .name, id: .cluster_id}]'

## Get nodepools

aliyun cs DescribeClusterNodePools --ClusterId c7fd0cb63683249cfa5068a4715a73ad4 | jq '.[][] | .nodepool_info | {name: .name, id: .nodepool_id}'

### Get nodes

aliyun cs DescribeClusterNodes --ClusterId c7fd0cb63683249cfa5068a4715a73ad4 | jq '.nodes[] | {name: .node_name, host: .host_name, pool: .nodepool_id, state: .state}'
