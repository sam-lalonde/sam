#!/usr/bin/env python
from datadog import initialize, api
import os
import sys


def main():
    options = {
        "api_key": sys.argv[1],
        "app_key": sys.argv[2],
    }
    initialize(**options)
    dashboard_id = "9hv-r28-pe7"
    results = api.Dashboard.get(dashboard_id)
    del results["id"]  # update API call cannot have this key
    results["template_variable_presets"] = []
    for c in ("c001", "c002", "c003"):
        for i in os.listdir("/home/slalonde/mob-deploy/mobius/{}".format(c)):
            results["template_variable_presets"].append({
                "name": "{} - {}".format(i, c),
                "template_variables": [
                    {"name": "school", "value": i},
                    {"name": "kube_deployment_webapp", "value": "{}-webapp".format(i)},
                    {"name": "kube_deployment_bt", "value": "{}-bt".format(i)},
                    {"name": "db", "value": i},
                    {"name": "ingress", "value": "{}-webapp".format(i)},
                    {"name": "kube_namespace", "value": i},
                ],
            })
    api.Dashboard.update(dashboard_id, **results)


if __name__ == "__main__":
    main()
