#!/usr/bin/env python3
"""
Usage: ./uptime_robot_update_stringmatch.py <api_key> <string_to_match> <action>
e.g.
./uptime_robot_update_stringmatch.py ABCD12345 c002 pause
"""
import logging
import requests
import sys

API_KEY = sys.argv[1]
STRING = sys.argv[2]  # string to match in monitor names
ACTION = sys.argv[3]  # pause or unpause

logging.basicConfig(
    format="%(asctime)s - %(levelname)s - %(message)s", level=logging.INFO
)


def main():
    monitors = get_monitors(API_KEY)
    monitor_ids = [x["id"] for x in monitors if STRING in x["friendly_name"]]
    for i in monitor_ids:
        update_monitor(API_KEY, i)

def get_monitors(api_key, include_logs=False):
    """ Returns a list of monitors from Uptime Robot """
    url = "https://api.uptimerobot.com/v2/getMonitors"
    headers = {
       "Content-Type": "application/x-www-form-urlencoded",
       "Cache-Control": "no-cache",
    }
    data = {
        "api_key": api_key
    }
    response = requests.post(url, headers=headers, data=data).json().get('pagination')
    total_record = int(response['total'])
    limit = int(response['limit'])

    monitors = []
    for offset in range(0, total_record, limit):
        data = {
            "api_key": api_key,
            "offset": offset,
            "logs": 1 if include_logs else 0,
        }
        response = requests.post(url, headers=headers, data=data).json().get("monitors")
        monitors.extend(response)
    return monitors

def update_monitor(api_key, monitor_id):
    if ACTION == "pause":
        status = 0
    elif ACTION == "unpause":
        status = 1
    else:
        logging.error("ERROR: Unknown action, exiting!")

    data = {
        "api_key": api_key,
        "id" : monitor_id,
        "status": status
    }

    headers = {
    "Content-Type": "application/x-www-form-urlencoded",
    "Cache-Control": "no-cache",
    }

    response = requests.post(
        "https://api.uptimerobot.com/v2/editMonitor", headers=headers, data=data,
    )

    """ Checking if the action was successful """
    if response.json().get("stat") != "ok":
        logging.error("Looks like something went wrong with {}".format(monitor_id))


if __name__ == "__main__":
    main()
