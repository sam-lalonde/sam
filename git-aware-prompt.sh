#!/bin/bash
# Install git-aware prompt
# https://github.com/jimeh/git-aware-prompt
if [ -d "$HOME/.bash/git-aware-prompt" ]; then
    echo "It looks like git-aware-prompt is already installed"
    exit
fi
mkdir ~/.bash
cd ~/.bash
git clone git://github.com/jimeh/git-aware-prompt.git
cd ~/sam
cat .git-aware-prompt-bashrc >> $HOME/.bashrc
source ~/.bashrc
