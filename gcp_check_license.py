#!/usr/bin/python3
"""
gcp_check_last_login.py - Check last login date for instances
"""
import argparse
import atexit
import psycopg2
import socket
import yaml
from modules import common
import logging
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)


def main():
    """ Compare system properties """
    common.remove_tmpdir(info.jira_ticket)
    sre_puppet = common.clone_repo(info.jira_ticket)
    atexit.register(common.remove_tmpdir, info.jira_ticket)
    logging.info("Collecting instances")
    instances = get_instances()
    logging.info("Collecting last logins (takes a while)")
    for instance in instances:
        check_student_licensing(instance)
    for instance in info.student_licensing_instances:
        check_activation_server_url(instance)
    # print(info.student_licensing_instances)
    # print("\ndate,instance,uid")
    # for r in sorted(info.last_logins):
    #     print("{},{},{}".format(*r))
    # print()


def get_instances():
    """ Get details about instances """
    yaml_file = socket.getfqdn().split("-controller")[0] + ".yaml"
    yaml_full_path = "/tmp/{}/sre-puppet/hieradata/mapleta/{}".format(info.jira_ticket, yaml_file)
    with open(yaml_full_path, "r") as f:
        cluster_yaml = yaml.safe_load(f)
    instances = sorted(cluster_yaml["mapleta"].get("instances"))
    return instances


def check_student_licensing(instance):
    db_params = common.get_db_params(info.jira_ticket, instance, info.cluster)
    try:
        db = common.Postgres(db_params)
        result = db.sql_exec(
            "SELECT value "
            "FROM system_properties "
            "WHERE key = 'ENABLE_STUDENT_LICENSING' "
            ";"
        )
    except psycopg2.DatabaseError as error:
        logging.error("{}Problem with SQL query!{}".format(common.LogColor.RED.value, common.LogColor.NOCOLOUR.value))
    finally:
        if db is not None:
            db.close()
    if result:
        if result[0][0] == 'true':
            info.student_licensing_instances.append(instance)

def check_activation_server_url(instance):
    db_params = common.get_db_params(info.jira_ticket, instance, info.cluster)
    try:
        db = common.Postgres(db_params)
        result = db.sql_exec(
            "SELECT value "
            "FROM system_properties "
            "WHERE key = 'ACTIVATION_SERVER_URL' "
            ";"
        )
    except psycopg2.DatabaseError as error:
        logging.error("{}Problem with SQL query!{}".format(common.LogColor.RED.value, common.LogColor.NOCOLOUR.value))
    finally:
        if db is not None:
            db.close()
    activation_server_url = None
    if result:
        activation_server_url = result[0][0]
    try:
        db = common.Postgres(db_params)
        result = db.sql_exec(
            "SELECT value "
            "FROM system_properties "
            "WHERE key = 'CLUSTERED' "
            ";"
        )
    except psycopg2.DatabaseError as error:
        logging.error("{}Problem with SQL query!{}".format(common.LogColor.RED.value, common.LogColor.NOCOLOUR.value))
    finally:
        if db is not None:
            db.close()
    clustered = None
    if result:
        clustered = result[0][0]
    print("{},{},{}".format(instance, clustered, activation_server_url))


class Info:
    """ Simple container for storing shared information """
    def __init__(self):
        self.cluster = socket.gethostname().split("-")[-2]
        self.jira_ticket = "SO-check-last-login"
        self.student_licensing_instances = []

if __name__ == "__main__":
    info = Info()
    main()
